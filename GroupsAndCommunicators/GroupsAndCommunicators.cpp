﻿// GroupsAndCommunicators.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "mpi.h"

using namespace std;

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    //int rank;
    //int membershipKey;

    //int* ranks1 = new int[2]{ 1, 2 };
    //int* ranks2 = new int[2]{ 3, 4 };

    ////Get basis group
    //MPI_Group basisGroup;
    //MPI_Comm_group(MPI_COMM_WORLD, &basisGroup);

    ////Get both groups
    //MPI_Group gr1, gr2;
    //MPI_Group_incl(basisGroup, 2, ranks1, &gr1);
    //MPI_Group_incl(basisGroup, 2, ranks2, &gr2);

    ////Create intra comms
    //MPI_Comm c1, c2;
    //MPI_Comm_create(MPI_COMM_WORLD, gr1, &c1);
    //MPI_Comm_create(MPI_COMM_WORLD, gr2, &c2);

    //MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    ////Merge intra into intro comm
    //MPI_Comm interComm;

    //if (rank % 2 == 0)
    //{
    //    MPI_Intercomm_create(c2, 2, c1, 1, 12, &interComm);
    //}
    //else MPI_Intercomm_create(c1, 1, c2, 2, 12, &interComm);

    //int rank;
    //MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //if (rank == 0)
    //{
    //    //Merge intra into intro comm
    //    MPI_Comm interComm;
    //    MPI_Intercomm_create(c1, 0, c2, 2, 1, &interComm);
    //}

  /*  if (rank == 0)
    {
        int size1, size2;
        MPI_Comm_size(c1, &size1);
        MPI_Comm_size(c2, &size2);

        cout << "First count is " << size1 << endl;
        cout << "Secound count is " << size2 << endl;

    }*/

    //MPI_Comm myComm;        // интра-коммуникатор локальной подгруппы
    //MPI_Comm myFirstComm;   // интер-коммуникатор
    //MPI_Comm mySecondComm;  // второй интер-коммуникатор (группа 1 только)

    //int rank;
    //int membershipKey;

    //MPI_Init(&argc, &argv);

    //MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //membershipKey = rank % 3;
    //// Построение интракоммуникатора для локальной подгруппы 
    //MPI_Comm_split(MPI_COMM_WORLD,membershipKey, rank, &myComm);

    //if (membershipKey == 0)
    //{
    //    // Группа 0 связывается с группой 1
    //    MPI_Intercomm_create(myComm, 0, MPI_COMM_WORLD, 1, 1, &myFirstComm);
    //}
    //else
    //    if (membershipKey == 1)
    //    {
    //        // Группа 1 связывается с группами 0 и 2
    //        MPI_Intercomm_create(myComm, 0, MPI_COMM_WORLD, 0, 1,
    //            &myFirstComm);
    //        MPI_Intercomm_create(myComm, 0, MPI_COMM_WORLD, 2, 12,
    //            &mySecondComm);
    //    }
    //    else
    //        if (membershipKey == 2)
    //        {
    //            // Группа 2 связывается с группа 1
    //            MPI_Intercomm_create(myComm, 0, MPI_COMM_WORLD, 1, 12,
    //                &myFirstComm);
    //        }
    //switch (membershipKey)
    //{
    //case 1:
    //    MPI_Comm_free(&mySecondComm);
    //case 0:
    //case 2:
    //    MPI_Comm_free(&myFirstComm);
    //    break;
    //}

    int flg;


    MPI_Comm com1 = MPI_COMM_WORLD, com2;
    MPI_Comm c1, c2;
    int size, rank;
    MPI_Comm_size(com1, &size);
    MPI_Comm_rank(com1, &rank);

    int color = rank % 2;

    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &com1);
    MPI_Intercomm_create(com1, 0, MPI_COMM_WORLD, 1 - color, 20, &c1);

    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &com2);
    MPI_Intercomm_create(com2, 0, MPI_COMM_WORLD, 1 - color, 20, &c2);

    MPI_Comm_test_inter(com2, &flg); 
    printf(" this comm %d \n", flg);
    MPI_Comm_free(&com1);
    MPI_Comm_free(&com2);

    //MPI close!
    MPI_Finalize();

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

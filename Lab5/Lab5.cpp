﻿#include <mpi.h>
#include <iostream>
#include <fstream>

#define MESSAGE_LEN 1001 

using namespace std;

MPI_Status status;
char* message;

//топология графа
int* fullMatrix;

//остовное дерево
int* skeletonMatrix;        

int index(int i, int j, int n)
{
    return i * n + j;
}


//заполняем матрицы
void init(int n) {
    ifstream f_in("full.txt");
    //ifstream s_in("skeleton.txt");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            f_in >> fullMatrix[index(i, j, n)];
            skeletonMatrix[index(i, j, n)] = 0;
            //s_in >> skeletonMatrix[index(i, j, n)];
        }
    }

    f_in.close();
    //s_in.close();
}

//печать матрицы
void print(int* matrix, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << matrix[index(i, j, n)] << " ";
        }
        cout << endl;
    }
}

bool IsBusy(int* matrix, int size, int start)
{
    int currentCol = start % size;

    int i = 0;
    while (i < size)
    {
        if (matrix[index(i, currentCol, size)] == 1) return true;
        else i++;
    }

    return false;
}

void master(int size, int rank) {
    init(size);

    cout << endl << "Full matrix: " << endl;
    print(fullMatrix, size);
	
    //отправляем всем процессам fullMatrix
    MPI_Bcast(fullMatrix, size * size, MPI_INT, 0, MPI_COMM_WORLD);

    cout << "First we must create skeleton matrix!" << endl;
	
    //Send skeleton matrix request to neighboors
    int sendCount = 0;
    for (int i = 0; i < size; i++) {
        if (fullMatrix[index(rank, i, size)] == 1) {
            MPI_Send(NULL, 0, MPI_CHAR, i, 0, MPI_COMM_WORLD);
            sendCount++;
            cout << "Send skeleton matrix request" << endl;
        }
    }

    //Add 1 to current rank
    if (sendCount == 0)
    {
        skeletonMatrix[index(status.MPI_SOURCE, rank, size)] = 1;
    }

    //Temp skeleton matrix
    int* tempMatrix = new int[size * size];

    //Get skeleton matrix response
    for (; sendCount; sendCount--) {

        MPI_Recv(tempMatrix, size * size, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (int i = 0; i < size * size; i++)
        {
            if (!IsBusy(skeletonMatrix, size, i)) skeletonMatrix[i] = tempMatrix[i];
        }
        //cout << "Slave recv (rank " << rank << ") is handled!" << endl;

    }

    cout << endl << "Skeleton matrix: " << endl;
    print(skeletonMatrix, size); 
}

void slave(int size, int rank) {

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            skeletonMatrix[index(i, j, size)] = 0;
        }
    }
	
    MPI_Bcast(fullMatrix, size * size, MPI_INT, 0, MPI_COMM_WORLD);

    //Get matrix skeleton response
    int recvCounts = 0;
    for (int i = 0; i < size; i++) 
    {
        if (fullMatrix[index(i, rank, size)] == 1) 
        {
            //cout << "Rank " << rank << "recv from " << i << endl;
            MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
            //cout << "Rank " << rank << "complete recv from " << i << endl;
            recvCounts++;
        }
    }

    cout << "Skeleton matrix request from " << status.MPI_SOURCE << " to " << rank << endl;

    //Continue matrix skeleton response
    int countSends = 0;
    for (int i = 0; i < size; i++) {
        if (fullMatrix[index(rank, i, size)] == 1) {
            MPI_Send(NULL, 0, MPI_CHAR, i, 0, MPI_COMM_WORLD);
            countSends++;
        }
    }

    skeletonMatrix[index(status.MPI_SOURCE, rank, size)] = 1;

    //Temp skeleton matrix
    int* tempMatrix = new int[size * size];
	
    //Get skeleton matrix response
    for (; countSends; countSends--) {

        MPI_Recv(tempMatrix, size * size, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    	for (int i = 0; i < size * size; i++)
    	{
            if (!IsBusy(skeletonMatrix, size, i)) skeletonMatrix[i] = tempMatrix[i];
   		}
		   //cout << "Slave recv (rank " << rank << ") is handled!" << endl;

    }

    cout << "Rank " << rank << endl;
    print(skeletonMatrix, size);

    for (int i = 0; i < size && recvCounts > 0; i++) 
    {
        if (fullMatrix[index(i, rank, size)] == 1) 
        {
            MPI_Send(skeletonMatrix, size * size, MPI_INT, i, 1, MPI_COMM_WORLD);
            recvCounts--;
        }
    }

	 //Send skeleton matrix to up 
    cout << "Rank "<< rank << " skeleton return!" << endl;

}

void SendMsg(int size, int rank)
{

    cout << "Second we must check skeleton matrix with message!" << endl;

    //отправляем всем процессам skeletonMatrix
    MPI_Bcast(skeletonMatrix, size * size, MPI_INT, 0, MPI_COMM_WORLD);

    //ведем сообщение на хосте
    cout << "Please input message: (max " << (MESSAGE_LEN - 1) / 2 << ")" << endl;
    message = new char[MESSAGE_LEN];
    cin.getline(message, MESSAGE_LEN);
    cout << endl;

    //отправляем сообщения первой строке
    int sendCount = 0;
    for (int i = 0; i < size; i++) {
        if (skeletonMatrix[index(rank, i, size)] == 1) {
            MPI_Send(message, MESSAGE_LEN, MPI_CHAR, i, 0, MPI_COMM_WORLD);
            sendCount++;
            cout << "Send message request" << endl;
        }
    }

    //получаем ответ от тех кому отправили
    while (sendCount) {

        //прием сообщений без данных
        MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
        sendCount--;
        cout << "Get response" << endl;
    }
}

void GetMsg(int size, int rank)
{
    MPI_Bcast(skeletonMatrix, size * size, MPI_INT, 0, MPI_COMM_WORLD);


    //печать полученого сообщения
    message = new char[MESSAGE_LEN];
    MPI_Recv(message, MESSAGE_LEN, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status); //
    cout << "From " << status.MPI_SOURCE << " to " << rank << ": '" << message << "'" << endl;


    //отправляем сообщения по рангу
    int countSends = 0;
    for (int i = 0; i < size; i++) {
        if (skeletonMatrix[index(rank, i, size)] == 1) {
            MPI_Send(message, MESSAGE_LEN, MPI_CHAR, i, 0, MPI_COMM_WORLD);
            countSends++;
            //cout << "Slave request (rank " << rank << ") is sended!" << endl;
        }
    }
    //получаем ответ от тех кому отправили
    for (; countSends; countSends--) {

        //получаем эхо ответ  
        MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        //cout << "Slave recv (rank " << rank << ") is handled!" << endl;

    }

    //рассылка сообщений вверх по вертикали
    MPI_Send(NULL, 0, MPI_INT, status.MPI_SOURCE, 1, MPI_COMM_WORLD);
    //cout << "Slave up request (rank " << rank << ") is sended!" << endl;
}

int main(int argc, char** argv)
{
    int size, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    //
  // 
    fullMatrix = new int[size * size];
    skeletonMatrix = new int[size * size];
	
    !rank ? master(size, rank) : slave(size, rank);
    !rank ? SendMsg(size, rank) : GetMsg(size, rank);

    MPI_Finalize();
    return 0;
}

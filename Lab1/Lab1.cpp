﻿// Lab1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>
#include <string>
#include <sstream>
#include "Matrix.h"
#include "mpi.h"

constexpr auto ROWS = 3;
constexpr auto COLS = 4;

using namespace std;

void MPI_Matrix_Multiply(Matrix mat1, Matrix mat2)
{
    int size, rank;

    //Get processes count
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size == mat1.rows + 1)
    {
        //Get process rank
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        //Rank is 0
        if (!rank)
        {

            /* Get our processes data */

            cout << "Matrix 1.";
            mat1.Print();

            cout << "Matrix 2.";
            mat2.Print();

            char str[100];

            MPI_Status status;

            int** resultMatrix = new int* [mat1.rows];

            //Get MPI messages with data
            for (int i = 0; i < mat1.rows; i++)
            {
                MPI_Recv(str, 100, MPI_CHAR, i + 1, 100, MPI_COMM_WORLD, &status);

                string numStr;
                stringstream stream(str);

                int* matrixRow = new int[mat1.rows];

                for (int j = 0; j < mat1.rows; j++)
                {
                    //Get our number
                    std::getline(stream, numStr, ' ');

                    //Convert to number
                    matrixRow[j] = std::stoi(numStr);

                    //cout << "Result output string is " << numStr << endl;
                }

                resultMatrix[i] = matrixRow;
            }

            Matrix result(resultMatrix, mat1.rows, mat1.rows);

            cout << "Your result is" << endl;
            result.Print();

        }
        else
        {
            /* Multiply one row to all columns */

            int index = rank - 1;
            const int resultLength = mat1.rows;

            //Used for message sending
            string resultStr;

            for (int i = 0; i < resultLength; i++)
            {
                int resultEl = 0;

                //Get one element
                for (int j = 0; j < mat2.rows; j++) resultEl += mat1.mat[index][j] * mat2.mat[j][i];

                resultStr += to_string(resultEl) + " ";
            }

            cout << "Result matrix row is " << resultStr << endl;

            //Send message to parent process
            MPI_Send(resultStr.c_str(), resultStr.size(), MPI_CHAR, 0, 100, MPI_COMM_WORLD);
        }
    }
    else cout << "Enter correct processes count!" << endl;
}

int main(int argc, char** argv)
{
    //MPI Initialize
    MPI_Init(&argc, &argv);

    Matrix mat1(4, 3), mat2(3, 4);
    
    MPI_Matrix_Multiply(mat2, mat1);

    //MPI close!
    MPI_Finalize();

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

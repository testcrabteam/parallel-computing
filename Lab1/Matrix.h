#pragma once
#include <ctime>
#include <iostream>

using namespace std;

class Matrix
{
private:
	int** mat;
public:
	int rows;
	int cols;

    //Spawn matrix with random values
	Matrix(int rows, int cols) : rows(rows), cols(cols)
	{
        srand(time(NULL));

        mat = new int* [rows];

        for (int i = 0; i < rows; i++)
        {
            mat[i] = new int[cols];

            for (int j = 0; j < cols; j++) mat[i][j] = rand() % 10;     //Random from 0 to 9
        }
	}

    //Create matrix by other int**
    Matrix(int** arr, int rows, int cols) : mat(arr), rows(rows), cols(cols)
    {}

    void Print()
    {
        if (mat)
        {

            cout << "Your matrix is: " << endl;

            for (int i = 0; i < rows; i++)
            {

                for (int j = 0; j < cols; j++)
                {
                    cout << mat[i][j] << " ";
                }

                cout << endl;
            }
        }
    }

    ~Matrix()
    {
        for (int i = 0; i < rows; i++) delete[] mat[i];
        delete[] mat;
    }

    friend void MPI_Matrix_Multiply(Matrix mat1, Matrix mat2);

};


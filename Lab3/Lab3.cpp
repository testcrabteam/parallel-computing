﻿// Необходимо реализовать алгоритм перемножения матриц ленточным способом с распределением строк.
// Execute task with 4x4 matrix

#include <iostream>
#include <ctime>
#include "mpi.h"

using namespace std;

const int ROOT_RANK = 0;
const int TAG = 10;
const MPI_Comm worldComm = MPI_COMM_WORLD;
const int SIZE = 4;

void PrintArr(int* arr, int length)
{
    for (int i = 0; i < length; i++) cout << arr[i] << " ";
    cout << endl;
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(worldComm, &rank);
    MPI_Comm graphComm;

    //Create graph topology
    {
        int index[] = { 4, 7, 10, 13, 16 };
        int edges[] = { 1, 2, 3, 4, 4, 0, 2, 1, 0, 3, 2, 0, 4, 3, 0, 1 };
        int nnodes = 5;
        MPI_Graph_create(worldComm, nnodes, index, edges, 0, &graphComm);
    }

    if (rank == ROOT_RANK) //HandleRootProcess(&graphComm);
    {
        cout << "Root process started" << endl;

        int mat1[SIZE][SIZE];
        int mat2[SIZE][SIZE];

        srand(time(NULL));

        for (int i = 0; i < SIZE; i++)
        {
            for (int j = 0; j < SIZE; j++) mat1[i][j] = rand() % 10;     //Random from 0 to 9
        }

        for (int i = 0; i < SIZE; i++)
        {
            for (int j = 0; j < SIZE; j++) mat2[i][j] = rand() % 10;     //Random from 0 to 9
        }

        cout << "Your first matrix is: " << endl;

        for (int i = 0; i < SIZE; i++)
        {

            for (int j = 0; j < SIZE; j++)
            {
                cout << mat1[i][j] << " ";
            }

            cout << endl;
        }

        cout << "Your second matrix is: " << endl;

        for (int i = 0; i < SIZE; i++)
        {

            for (int j = 0; j < SIZE; j++)
            {
                cout << mat2[i][j] << " ";
            }

            cout << endl;
        }


        int result[SIZE][SIZE];

        //Send every matrixes row to ranks
        for (int i = 0; i < SIZE; i++)
        {
            MPI_Send(mat1 + i, 4, MPI_INT, i + 1, TAG, worldComm);

            int col[SIZE + 1];
            col[0] = i;
        	for (int j = 0; j < SIZE; j++)
        	{
                col[j + 1] = mat2[j][i];
        	}
            MPI_Send(col, 5, MPI_INT, i + 1, TAG, worldComm);
        }

        //Get our results
        for (int i = 0; i < 4; i++)
        {
            MPI_Recv(result + i, 4, MPI_INT, i + 1, TAG, worldComm, MPI_STATUS_IGNORE);
        }

        //Print our result
        cout << "Your result is: " << endl;
        for (int i = 0; i < SIZE; i++)
        {

            for (int j = 0; j < SIZE; j++)
            {
                cout << result[i][j] << " ";
            }

            cout << endl;
        }
    }
    else
    {
        cout << "Calculation process started" << endl;

        int firstRow[SIZE];
        int secondColumn[SIZE + 1]; 
        int resultRow[SIZE] = { 0 };
        int currentColumn = 0;

        int prevRank = (rank > 1) ? rank - 1 : SIZE;
        int nextRank = (rank < SIZE) ? rank + 1 : 1;

        //Get rows
        MPI_Recv(firstRow, 4, MPI_INT, ROOT_RANK, TAG, worldComm, MPI_STATUS_IGNORE);   
        MPI_Recv(secondColumn, 5, MPI_INT, ROOT_RANK, TAG, worldComm, MPI_STATUS_IGNORE);
    	
        int currentRow = rank;

        MPI_Status status;
        
        
        for (int j = 0; j < SIZE; j++)
        {
            currentColumn = secondColumn[0];

            if (rank == 1)
            {
                cout << "Iteration " << j << endl;
                cout << "I am rank " << rank << endl;
                cout << "Current column is " << currentColumn << endl;
            }
            
            for (int i = 0; i < SIZE; i++)
            {
                resultRow[currentColumn] += firstRow[i] * secondColumn[i + 1];
            }

            if (rank == 1)
            {
                cout << "Iteration first row is ";
                PrintArr(resultRow, SIZE);
            }

            MPI_Send(secondColumn, 5, MPI_INT, nextRank, TAG, worldComm);

            
            //if (rank == 1) PrintArr(resultRow, SIZE);
            MPI_Recv(secondColumn, 5, MPI_INT, prevRank, TAG, worldComm, &status);
        	//if (rank == 1) PrintArr(resultRow, SIZE);
            
            
            //cout << "Calculated!" << rank << "+" << j << endl;
        }

        MPI_Send(resultRow, 4, MPI_INT, ROOT_RANK, TAG, worldComm);
    }

    MPI_Finalize();

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

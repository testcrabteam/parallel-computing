﻿// Lab2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>
#include <array>
#include <algorithm>
#include "mpi.h"

using namespace std;

void Print(double** mat, int rows, int cols)
{
    if (mat)
    {

        cout << "Your matrix is: " << endl;

        for (int i = 0; i < rows; i++)
        {

            for (int j = 0; j < cols; j++)
            {
                cout.precision(2);
                cout << mat[i][j] << " ";
            }

            cout << endl;
        }
    }
}

double GetExpression(double x, double y)
{
    return sin(x) + exp(y);
}

double** GetMatrixByExpressionValues(int start, int end, double step, int& rows, int& cols)
{
    double** matrixA;

    rows = cols = ((double)end - start) / step;

    //Init matrix memory
    matrixA = new double* [rows];
    for (int i = 0; i < rows; i++) matrixA[i] = new double[cols];

    int row = 0;

    //Calculation
    for (double x = start; x < end; x += step)
    {
        if (row >= rows) break;

        int col = 0;

        for (double y = start; y < end; y += step)
        {
            matrixA[row][col++] = GetExpression(x, y);
            if (col >= cols) break;
        }

        row++;
    }

    Print(matrixA, rows, cols);

    return matrixA;
}

double* GetMatrixSegments(double** mat, int rows, int cols)
{
    double* res = new double[rows * cols];

    int ind = 0;
    for (int i = 0; i < rows; i += 2)
    {
        for (int j = 0; j < cols; j += 2)
        {
            res[ind] = mat[i][j];
            res[ind + 1] = mat[i][j + 1];
            res[ind + 2] = mat[i + 1][j + 1];
            res[ind + 3] = mat[i + 1][j];
            ind += 4;
        }
    }

    return res;
}

double GetArrMin(double* arr, int length)
{
    double min = arr[0];

    for (int i = 0; i < length; i++)
    {
        if (arr[i] < min) min = arr[i];
    }

    return min;
}

double GetArrMax (double* arr, int length)
{
    double max = arr[0];

    for (int i = 0; i < length; i++)
    {
        if (arr[i] < max) max = arr[i];
    }

    return max;
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
   
    int size, rank;

    MPI_Comm comWorld = MPI_COMM_WORLD;

    MPI_Comm_size(comWorld, &size);
    MPI_Comm_rank(comWorld, &rank);

    int color = (rank == 0) ? 2 : rank & 1;
    
    /* Create 3 groups - root, max calculation and min calculation */

    MPI_Comm comm;
    MPI_Comm_split(comWorld, color, rank, &comm);

    switch (color)
    {
    case 0:

    {
        /* For the minimum */

        MPI_Comm rootComm;
        MPI_Intercomm_create(comm, 0, comWorld, 0, 55, &rootComm);

        double segment[4];

        MPI_Scatter(nullptr, 4, MPI_DOUBLE, segment, 4, MPI_DOUBLE, 0, rootComm);
        MPI_Barrier(rootComm);

        //Send local min to root process
        MPI_Reduce(std::min_element(segment, segment + 4), nullptr, 1, MPI_DOUBLE, MPI_MIN, 0, rootComm);
    }

        break;
    case 1:

    {
        /* For the maximum */

        MPI_Comm localComm;
        MPI_Intercomm_create(comm, 0, comWorld, 0, 100, &localComm);

        double segment[4];

        MPI_Scatter(nullptr, 4, MPI_DOUBLE, segment, 4, MPI_DOUBLE, 0, localComm);
        MPI_Barrier(localComm);

        double* max = std::max_element(segment, segment + 4);

        //Send local max to root process
        MPI_Reduce(max, nullptr, 1, MPI_DOUBLE, MPI_MAX, 0, localComm);
    }

        break;
    case 2: 

    {
        /* For the root process */

        MPI_Comm maxComm;
        MPI_Comm minComm;
        MPI_Intercomm_create(comm, rank, comWorld, 1, 100, &maxComm);
        MPI_Intercomm_create(comm, rank, comWorld, 2, 55, &minComm);

        const int maxSize = 100;

        double** matrixA;
        double* segments = new double[maxSize];
        double* rankSegments = new double[4];
        int rows, cols;

        /* Define system parameters */

        const int start = 0;
        const int end = 1;
        const double step = 0.1;

        matrixA = GetMatrixByExpressionValues(start, end, step, rows, cols);

        /* Create matrix segments 2x2 in linear array */

        delete[] segments;
        segments = GetMatrixSegments(matrixA, rows, cols);
        //cout << "\nSending segments: " << endl;
        //for (int i = 0; i < rows * cols; i++) cout << segments[i] << " ";

        /* Send segments to max group */

        MPI_Scatter(segments, 4, MPI_DOUBLE, nullptr, 4, MPI_DOUBLE, MPI_ROOT, maxComm);
        MPI_Barrier(maxComm);
        cout << "Segments was sent to find maximum!" << endl;

        /* Send segments to min group */

        MPI_Scatter(segments, 4, MPI_DOUBLE, nullptr, 4, MPI_DOUBLE, MPI_ROOT, minComm);
        MPI_Barrier(minComm);
        cout << "Segments was sent to find minimum!" << endl;

        double resMax;
        double resMin;

        //Get max
        MPI_Reduce(nullptr, &resMax, 1, MPI_DOUBLE, MPI_MAX, MPI_ROOT, maxComm);

        //Get min
        MPI_Reduce(nullptr, &resMin, 1, MPI_DOUBLE, MPI_MIN, MPI_ROOT, minComm);

        cout << "Your maximum is " << resMax << endl;
        cout << "Your minimum is " << resMin << endl;
    }

        break;
    default:
        break;
    }

    MPI_Finalize();
    
    return 0;
}



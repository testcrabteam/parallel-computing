﻿// HelloWorld.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <chrono>
#include <thread>
#include "mpi.h"

using namespace std;

int main(int argc, char** argv)
{
    //MPI Initialize
    MPI_Init(&argc, &argv);

    int size, rank;

    //Get process count in @size
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    //Get process rank in @rank
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::cout << "Hello World!\n";
    std::cout << "That process have " << rank << "rank in " << size << " of another!\n";

    this_thread::sleep_for(chrono::milliseconds(rank * 500));

    //Like thread.join(), sync all processes
    MPI_Barrier(MPI_COMM_WORLD);

    std::cout << "End of program!\n\n";

    //MPI close!
    MPI_Finalize();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

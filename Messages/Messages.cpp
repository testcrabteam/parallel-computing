﻿// Messages.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "mpi.h"

using namespace std;


int main(int argc, char** argv)
{
    //MPI Initialize
    MPI_Init(&argc, &argv);

    int size, rank;
    char msg[20] = "Hello World";

    int startRank = 0, destRank = 1;

    MPI_Status status;

    //Get process rank in @rank
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    switch (rank)
    {
    case 0:
        MPI_Send(msg, strlen(msg) + 1, MPI_CHAR, destRank, 100, MPI_COMM_WORLD);
        cout << "Message was sended!" << endl;

        break;
    case 1:
        MPI_Recv(msg, 20, MPI_CHAR, startRank, 100, MPI_COMM_WORLD, &status);

        cout << "Your message is " << msg << endl;
        cout << "Your source is " << status.MPI_SOURCE << endl;
        cout << "Your destination is " << rank << endl;
        cout << "Your tag is " << status.MPI_TAG << endl;
        cout << "Your error is " << status.MPI_ERROR << endl;

        break;
    default: 
        break;
    }

    //MPI close!
    MPI_Finalize();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

﻿#include <iostream>
#include <mpi.h>
#include <iomanip>
#include <queue>
#include <fstream>
#include <ctime>

const int TagForClient = 1;
const int TagForResourse = 2;
const int RequestLength = 5;
const int ResourcesCount = 2;

//
enum Resource {
	First,
	Second
};

//Client want to capture or to release the resource
enum RequestType {
	Capture,
	Release
};

//Is resource used by client, or free
enum ResourceStatus {
	Busy,
	Free
};

//Used for server request-response system
struct ResourceInformation {
	int ClientId;
	int ResourceId;
	ResourceStatus ResourceStatus = Free;
};

//Contains clientId, resourceId, requestType (schema1) and summands in schema 2
typedef int Request[RequestLength];

//Server Rank number
#define SERVER 0

//Resource rank
#define RESOURCE 1

//Summands count
#define SUMMANDS_LEN 3

using namespace std;

int summands[SUMMANDS_LEN];
int result;

void SchemaFirst(int, MPI_Status&);
void SchemaSecond(int, MPI_Status&);

struct Client
{
	int clientId;
	int resourceId;
	int* summands;
};

//Resource Procedure
int GetSum(int summands[], int length)
{
	int result = 0;
	for (int i = 0; i < length; i++)
	{
		result += summands[i];
	}
	return result;
}

void SchemaFirst(int processId, MPI_Status& status)
{
	cout << "-------------Start SchemaFirst-------------" << endl;

	int localSummands[SUMMANDS_LEN];
	int isFree;

	Request request;
	ResourceInformation* resourcesInformation[ResourcesCount] = {
		new ResourceInformation(),
		new ResourceInformation()
	};

	switch (processId)
	{
	case SERVER:
		while (true)
		{
			cout << "[Server] Request Waiting." << endl;

			//Get requests from any sources
			MPI_Recv(&request, RequestLength, MPI_INT, MPI_ANY_SOURCE, TagForClient, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			int clientId = request[0];
			int resourceId = request[1];
			int requestType = request[2];

			cout << "[Server] Received request from client [" << clientId << "]." << endl;

			if (requestType == Capture)
			{
				cout << "[Server] Client [" << clientId << "] wants to CAPTURE resource [" << resourceId << "]." << endl;

				if (resourcesInformation[resourceId]->ResourceStatus == Free)	//Capture resource
				{
					//Send resource statement to client
					MPI_Send(&resourcesInformation[resourceId]->ResourceStatus, 1, MPI_INT, clientId, TagForClient, MPI_COMM_WORLD);

					cout << "[Server] Sent status [Free] of resource to client [" << clientId << "]." << endl;

					resourcesInformation[resourceId]->ResourceStatus = Busy;
					resourcesInformation[resourceId]->ClientId = clientId;
					resourcesInformation[resourceId]->ResourceId = resourceId;

					cout << "[Server] The resource [" << resourceId << "] was captured." << endl;
				}
				else	//Don't touch busy resource
				{
					MPI_Send(&resourcesInformation[resourceId]->ResourceStatus, 1, MPI_INT, clientId, TagForClient, MPI_COMM_WORLD);

					cout << "[Server] Sent status [Busy] of resource to client [" << clientId << "]." << endl;
					cout << "[Server] The resource [" << resourceId << "] is busy." << endl;
				}
			}
			else if (requestType == Release)	//Release our resource
			{
				cout << "[Server] Client [" << clientId << "] wants to RELEASE resource [" << resourceId << "]." << endl;

				resourcesInformation[resourceId]->ResourceStatus = Free;

				cout << "[Server] The resource [" << resourceId << "] was released." << endl;
			}
		}
		break;

	case RESOURCE:
		while (true)
		{
			//Get summands from any clients
			MPI_Recv(&summands, SUMMANDS_LEN, MPI_INT, MPI_ANY_SOURCE, TagForClient, MPI_COMM_WORLD, &status);

			cout << "[Resource] Received data from client [" << status.MPI_SOURCE << "]." << endl;
			result = GetSum(summands, SUMMANDS_LEN);
			cout << "[Resource] Calculated the result." << endl;

			//Send result to source
			MPI_Send(&result, 1, MPI_INT, status.MPI_SOURCE, TagForClient, MPI_COMM_WORLD);

			cout << "[Resource] Sent result to client [" << status.MPI_SOURCE << "]." << endl;
		}
		break;

	default:	//Any client handling

		srand(time(NULL) + processId);

		//Generate summands
		for (int i = 0; i < SUMMANDS_LEN; i++)
		{
			localSummands[i] = rand() % 150;
		}

		cout << "[Client " << processId << "] Our summands are ";
		for (int i = 0; i < SUMMANDS_LEN; i++)
		{
			cout << localSummands[i] << ", ";
		}
		cout << endl;

		request[0] = processId;
		request[1] = Resource::First;
		request[2] = Capture;

		while (true)
		{
			//Send request to server
			MPI_Send(&request, RequestLength, MPI_INT, SERVER, TagForClient, MPI_COMM_WORLD);

			cout << "[Client " << processId << "] Requested access (for CAPTURE) to the resource [" << request[1] << "]." << endl;

			//Get resource status from server
			MPI_Recv(&isFree, 1, MPI_INT, SERVER, TagForClient, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			cout << "[Client " << processId << "] Received status of resource [" << request[1] << "] from server." << endl;

			//End requests sending if resource is free
			if (isFree)
			{
				cout << "[Client " << processId << "] Access is allowed." << endl;
				break;
			}

			cout << "[Client " << processId << "] Access is denied." << endl;
		}

		//Send arguments to resource for calculation
		MPI_Send(&localSummands, SUMMANDS_LEN, MPI_INT, RESOURCE, TagForClient, MPI_COMM_WORLD);

		cout << "[Client " << processId << "] Sent data to resource [" << request[1] << "] for calculation." << endl;

		//Get result from resource
		MPI_Recv(&result, 1, MPI_INT, RESOURCE, TagForClient, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		cout << "[Client " << processId << "] Received result of calculation from resource [" << request[1] << "]." << endl;

		//Do release request to server
		request[2] = Release;
		MPI_Send(&request, RequestLength, MPI_INT, SERVER, TagForClient, MPI_COMM_WORLD);

		cout << "[Client " << processId << "] Sent request to RELEASE the resource [" << request[1] << "]." << endl;
		cout << "[Client " << processId << "] Result = " << result << "." << endl;
		break;
	}

}

void SchemaSecond(int processId, MPI_Status& status)
{
	cout << "-------------Start SchemaSecond-------------" << endl;
	int localSummands[SUMMANDS_LEN];
	queue<Client> queue;
	Request request;
	int resourceIsReady;
	int clientIsReady;
	int clientCount;
	MPI_Comm_size(MPI_COMM_WORLD, &clientCount);
	clientCount -= 2;

	ResourceInformation* resourcesInformation[ResourcesCount] = {
		new ResourceInformation(),
		new ResourceInformation()
	};

	switch (processId)
	{
	case SERVER:
		while (true)
		{
			//Stop handling if 
			if (clientCount <= 0)
			{
				system("pause");
			}

			cout << "[Server] Request Waiting." << endl;

			//Try get request from client
			MPI_Iprobe(MPI_ANY_SOURCE, TagForClient, MPI_COMM_WORLD, &clientIsReady, MPI_STATUS_IGNORE);

			if (clientIsReady)
			{
				//Get request from any client
				MPI_Recv(&request, RequestLength, MPI_INT, MPI_ANY_SOURCE, TagForClient, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

				Client* client = new Client();
				client->clientId = request[0];
				client->resourceId = request[1];
				client->summands = new int[SUMMANDS_LEN] { request[2], request[3], request[4] };

				//If client made request, he's going to the queue
				queue.push(*client);
				cout << "[Server] Client [" << client->clientId << "] placed to queue." << endl;
			}
			if (!queue.empty())
			{
				Client currentClient = queue.front();

				if (resourcesInformation[currentClient.resourceId]->ResourceStatus == Free)
				{
					Request currentRequest =
					{
						currentClient.clientId,
						currentClient.summands[0],
						currentClient.summands[1],
						currentClient.summands[2]
					};

					//Send summands to resource
					MPI_Send(&currentRequest, RequestLength, MPI_INT, RESOURCE, TagForResourse, MPI_COMM_WORLD);

					resourcesInformation[currentClient.resourceId]->ResourceStatus = Busy;
					resourcesInformation[currentClient.resourceId]->ClientId = currentClient.clientId;
					resourcesInformation[currentClient.resourceId]->ResourceId = currentClient.resourceId;

					cout << "[Server] The resource [" << currentClient.resourceId << "] was captured." << endl;
				}
				
				//Try get result from resource
				MPI_Iprobe(RESOURCE, TagForResourse, MPI_COMM_WORLD, &resourceIsReady, MPI_STATUS_IGNORE);

				if (resourceIsReady)
				{
					currentClient = queue.front();

					//Get result by resource
					MPI_Recv(&request, RequestLength, MPI_INT, RESOURCE, TagForResourse, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					cout << "[Server] Received result from resource for client [" << request[0] << "]." << endl;

					//Send result to client
					MPI_Send(&request[1], 1, MPI_INT, request[0], TagForClient, MPI_COMM_WORLD);
					cout << "[Server] Sent result to client [" << request[0] << "]." << endl;

					resourcesInformation[currentClient.resourceId]->ResourceStatus = Free;
					queue.pop();
					clientCount--;
				}
			}
		}
		break;

	case RESOURCE:
		while (true)
		{
			//Get summands by server
			MPI_Recv(&request, RequestLength, MPI_INT, MPI_ANY_SOURCE, TagForResourse, MPI_COMM_WORLD, &status);
			cout << "[Resource] Received data from server (for client [" << request[0] << "])." << endl;

			//Calculation result
			int summands[] = { request[1], request[2], request[3] };
			result = GetSum(summands, SUMMANDS_LEN);
			cout << "[Resource] Calculated the result." << endl;

			//Send result to server
			request[1] = result;
			MPI_Send(&request, RequestLength, MPI_INT, SERVER, TagForResourse, MPI_COMM_WORLD);
			cout << "[Resource] Sent result to server (for client [" << request[0] << "])." << endl;
		}
		break;

	default:
		srand(time(NULL) + processId);

		//Create summands
		for (int i = 0; i < SUMMANDS_LEN; i++)
		{
			request[i + 2] = localSummands[i] = rand() % 150;
		}

		request[0] = processId;
		request[1] = Resource::First;

		//Send request to server from client
		MPI_Send(&request, RequestLength, MPI_INT, SERVER, TagForClient, MPI_COMM_WORLD);
		cout << "[Client " << processId << "] Sent request to server." << endl;

		//Get end result from server
		MPI_Recv(&result, 1, MPI_INT, SERVER, TagForClient, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		cout << "[Client " << processId << "] Received result from server." << endl;
		cout << "[Client " << processId << "] Result = " << result << "." << endl;
		break;
	}
}


int main(int argc, char** argv)
{
	MPI_Init(&argc, &argv);
	int proccessId;
	MPI_Status status;
	MPI_Comm_rank(MPI_COMM_WORLD, &proccessId);
	SchemaFirst(proccessId, status);
	//SchemaSecond(proccessId, status);
	MPI_Finalize();
	return 0;
}
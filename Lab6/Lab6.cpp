﻿#include <cstdio>
#include <cstdlib>
#include "mpi.h"
#include <iostream>
#include <iomanip>
#include <ctime>

using namespace std;
const int DigitsCount = 16;
const int length = 2;


void PrintArr(int* arr, int length)
{
	if (length > 0)
	{
		for (int i = 0; i < length; i++)
		{
			cout << arr[i] << " ";
		}
		cout << endl;
	}
	else cout << "Array is empty!" << endl;
}

int main(int argc, char* argv[]) {
	unsigned int start_time;
	int rank, size;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm hyperCoub;
	
	int* rankSource = new int[1];
	int* rankDest = new int[1];
	
	const int BlockSize = (int)DigitsCount / size;
	const int DimsCount = 3;
	const int BigBlockSize = BlockSize * 2;
	
	int mainVector[DigitsCount];
	int temp = 0;

	int mid = (int)BigBlockSize / 2;
	int left = 0;
	int right = BigBlockSize;

	int* ownVector = new int[BlockSize];
	int* hypercubeVector = new int[16];
	int* neighborVectorPiece = new int[BlockSize];
	int* bothVector = new int[BigBlockSize];
	int* buf = new int[BigBlockSize];
	int* SmallBuf = new int[BlockSize];
	int* result = new int[BigBlockSize];
	
	int dims[3] = { 2,2,2 };
	int periods[3] = { 1,1,1 };
	MPI_Cart_create(MPI_COMM_WORLD, 3, dims, periods, 1, &hyperCoub);

	// Create array
	if (rank == 0) 
	{
		srand(time(0));
		start_time = clock();
		
		cout << "Initial array: " << endl;
		for (int i = 0; i < DigitsCount; i++) 
		{
			//mainVector[i] = DigitsCount-i;
			mainVector[i] = rand() % 50;
			cout << setw(length) << mainVector[i] << " ";
		}
		cout << endl;
	}

	MPI_Scatter(mainVector, BlockSize, MPI_INT, ownVector, BlockSize, MPI_INT, 0, hyperCoub);

	if (rank == 0) cout << endl << "First - hypercube operations!!" << endl << endl;

	// БЛОК РАБОТЫ С ГИПЕРКУБОМ

	for (int i = 0; i < DimsCount; i++)
	{
		
		MPI_Cart_shift(hyperCoub, i, 1, &rankSource[0], &rankDest[0]);

		if (rank == 0)
		{
			cout << "Iteration " << i;
			cout << ": rank " << rank << " destination is " << rankDest[0] << endl;
		}
		
		if (rankSource[0] > rank) 
		{
			MPI_Recv(neighborVectorPiece, BlockSize, MPI_INT, rankSource[0], 99, hyperCoub, &status);

			//Concat rank and neighbor vectors
			for (int j = 0; j < BlockSize; j++) {
				bothVector[j] = ownVector[j];
				bothVector[j + BlockSize] = neighborVectorPiece[j];
			}

			if (rank == 0)
			{
				cout << endl;
				cout << "Before: " << endl;
				cout << "Rank " << rank << " own vector: ";
				PrintArr(ownVector, BlockSize);
				cout << "Rank " << rank << " neighboor vector: ";
				PrintArr(neighborVectorPiece, BlockSize);
			}
			
			int iterationLeft = 0;
			int iterationRight = 0;

			//Simple sort of blocks
			if (bothVector[left] > bothVector[right - 1]) 
			{
				for (int i = 0; i < BlockSize; i++) {
					result[i] = bothVector[mid + i];
					result[mid + i] = bothVector[i];
				}
			}
			//Parallel shell's sort across blocks
			else {
				while (((left + iterationLeft) < mid) && ((mid + iterationRight) < right)) {
					if (bothVector[left + iterationLeft] < bothVector[mid + iterationRight]) {
						result[iterationLeft + iterationRight] = bothVector[left + iterationLeft];
						iterationLeft++;
					}
					else {
						result[iterationLeft + iterationRight] = bothVector[mid + iterationRight];
						iterationRight++;
					}
				}
				while (left + iterationLeft < mid) {
					result[iterationLeft + iterationRight] = bothVector[left + iterationLeft];
					iterationLeft++;
				}

				while (mid + iterationRight < right) {
					result[iterationLeft + iterationRight] = bothVector[mid + iterationRight];
					iterationRight++;
				}
			}
			for (int i = 0; i < BigBlockSize; i++) {
				bothVector[left + i] = result[i];
			}

			/*cout << "Rank " << rank << " - Result vector: ";
			PrintArr(result, BigBlockSize);*/

			//Replace parameters
			for (int j = 0; j < BlockSize; j++) {
				ownVector[j] = bothVector[j];
				neighborVectorPiece[j] = bothVector[BlockSize + j];
			}

			if (rank == 0)
			{
				cout << endl;
				cout << "After: " << endl;
				cout << "Rank " << rank << " own vector: ";
				PrintArr(ownVector, BlockSize);
				cout << "Rank " << rank << " neighboor vector: ";
				PrintArr(neighborVectorPiece, BlockSize);
			}

			//Send neighbor to destination
			MPI_Send(neighborVectorPiece, BlockSize, MPI_INT, rankDest[0], 98, hyperCoub);
		}
		else {
			MPI_Send(ownVector, BlockSize, MPI_INT, rankDest[0], 99, hyperCoub);
			MPI_Recv(ownVector, BlockSize, MPI_INT, rankSource[0], 98, hyperCoub, &status);
		}
	}

	MPI_Gather(ownVector, BlockSize, MPI_INT, hypercubeVector, BlockSize, MPI_INT, 0, hyperCoub);

	if (rank == 0)
	{
		cout << endl << "Hypercube vector: ";
		PrintArr(hypercubeVector, 16);
		cout << endl << endl; 
	}

	// БЛОК ЧЕТ/НЕЧЕТ РЕАЛИЗАЦИИ
	for (int k = 0; k < 4; k++) 
	{
		if (rank == 0)
		{
			cout << "Iteration " << k << endl;
		}
		
		for (int i = 0; i < 2; i++) 
		{
			if (((rank + i) % 2 == 0) && (rank < (size - 1))) 
			{
				MPI_Recv(neighborVectorPiece, BlockSize, MPI_INT, rank + 1, 99, hyperCoub, &status);

				//Concat own and neighbor vectors
				for (int j = 0; j < BlockSize; j++) {
					bothVector[j] = ownVector[j];
					bothVector[j + BlockSize] = neighborVectorPiece[j];
				}

				if (rank == 0)
				{
					cout << "Swap between ranks " << rank << " and " << rank + 1;
					cout << endl;
					cout << "Before: " << endl;
					cout << "Rank " << rank << " own vector: ";
					PrintArr(ownVector, BlockSize);
					cout << "Rank " << rank << " neighboor vector: ";
					PrintArr(neighborVectorPiece, BlockSize);
				}

				//
				for (int step = BigBlockSize / 2; step > 0; step /= 2) {
					for (int k = step; k < BigBlockSize; k++) {
						for (int j = k - step; j >= 0 && bothVector[j] > bothVector[j + step]; j -= step) {
							if (bothVector[j] > bothVector[j + 1]) {
								temp = bothVector[j];
								bothVector[j] = bothVector[j + 1];
								bothVector[j + 1] = temp;
							}
						}
					}
				}

				for (int j = 0; j < BlockSize; j++) {
					ownVector[j] = bothVector[j];
					neighborVectorPiece[j] = bothVector[BlockSize + j];
				}

				if (rank == 0)
				{
					cout << endl;
					cout << "After: " << endl;
					cout << "Rank " << rank << " own vector: ";
					PrintArr(ownVector, BlockSize);
					cout << "Rank " << rank << " neighboor vector: ";
					PrintArr(neighborVectorPiece, BlockSize);
				}
				
				MPI_Send(neighborVectorPiece, BlockSize, MPI_INT, rank + 1, 98, hyperCoub);
			}
			else
				if ((rank > 0) && ((rank + i) != size)) {
					MPI_Send(ownVector, BlockSize, MPI_INT, rank - 1, 99, hyperCoub);
					MPI_Recv(ownVector, BlockSize, MPI_INT, rank - 1, 98, hyperCoub, &status);
				}
		}
	}
	MPI_Gather(ownVector, BlockSize, MPI_INT, mainVector, BlockSize, MPI_INT, 0, hyperCoub);

	if (rank == 0) {
		cout << "Result array:" << endl;
		for (int i = 0; i < DigitsCount; i++) {
			cout << setw(length) << mainVector[i] << " ";
		}
		unsigned int end_time = clock();
		unsigned int search_time = end_time - start_time;
		cout << endl << "Shell's sort parallel execution";
		cout << endl << "Execution time: " << search_time / 1000.0 << "\nSorted " << DigitsCount << " of elements" << endl;
	}
	MPI_Comm_free(&hyperCoub);
	MPI_Finalize();
	delete[]rankSource;
	delete[]rankDest;
	delete[]ownVector;
	delete[]neighborVectorPiece;
	delete[]bothVector;
	delete[]buf;
	delete[]SmallBuf;
	delete[]result;
	return 0;
}
